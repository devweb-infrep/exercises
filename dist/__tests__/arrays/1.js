import remove from './../../exercises/arrays/1';
describe('remove (remove.js)', () => {
  const source = ['a', 0, 'word', 1337];

  it('should remove strings', () => {
    expect(remove(source, 'a')).toEqual([0, 'word', 1337]);
  });

  it('should remove numbers', () => {
    expect(remove(source, 0)).toEqual(['a', 'word', 1337]);
  });

  it('should remove multiplis args', () => {
    expect(remove(source, 'a', 'word', 1337)).toEqual([0]);
  });
});
