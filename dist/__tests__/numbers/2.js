import isEven from './../../exercises/numbers/2';

describe('isEven (is-even.js)', () => {
  test('should return true if passed number is even and false if is not', () => {
    expect(isEven(234)).toBe(true);
    expect(isEven(33)).toBe(false);
    expect(isEven(-2)).toBe(true);
  });
});
