import addition from './../../exercises/numbers/1';

describe('addition (addition.js)', () => {
  it('should work', () => {
    expect(addition(2, 2)).toBe(4);
    expect(addition(2, -2)).toBe(0);
    expect(addition(-2, -2)).toBe(-4);
  });
});
