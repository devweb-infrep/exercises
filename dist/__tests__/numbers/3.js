import leapYears from './../../exercises/numbers/3';

describe('leapYears (leap-years.js)', () => {
  it('should work', () => {
    expect(leapYears(0)).toBe(true);

    expect(leapYears(1988)).toBe(true);

    expect(leapYears(2000)).toBe(true);
    expect(leapYears(1700)).toBe(false);
    expect(leapYears(1900)).toBe(false);
  });
});
