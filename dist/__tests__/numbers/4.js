import fibonacci from './../../exercises/numbers/4';

describe('fibonacci (fibonacci.js)', () => {
  it('should work', () => {
    expect(fibonacci(4)).toBe(3);
    expect(fibonacci(16)).toBe(987);
  });
});
