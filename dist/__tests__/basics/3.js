import min from './../../exercises/basics/3';

describe('min (min.js)', () => {
  it('should return the minimum of two given numbers', () => {
    expect(min(0, 0)).toBe(0);
    expect(min(-1, 1)).toBe(-1);
    expect(min(2, 5)).toBe(2);
    expect(min(-1, -2)).toBe(-2);
    expect(min(-2, -1)).toBe(-2);
  });
});
