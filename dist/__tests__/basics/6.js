import area from './../../exercises/basics/6';

describe('area (area.js)', () => {
  it('should calculate the area of a circle', () => {
    expect(area(10)).toBe(Math.PI * 100);
  });
});
