import sayHello from '../../exercises/basics/1';

describe('sayHello (welcome.js)', () => {
  it('should return the correct welcome message', () => {
    expect(sayHello('Jean', 'Dupont')).toBe('Hello Jean Dupont !');
  });
});
