import perimeter from './../../exercises/basics/5';

describe('perimeter (perimeter.js)', () => {
  it('should calculate the perimeter of a circle', () => {
    expect(perimeter(10)).toBe(Math.PI * 2 * 10);
  });
});
