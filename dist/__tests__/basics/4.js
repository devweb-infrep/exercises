import calc from './../../exercises/basics/4';

describe('calc (calculate.js)', () => {
  it('should handle additions', () => {
    expect(calc(0, '+', 0)).toBe(0);
    expect(calc(-1, '+', 1)).toBe(0);
    expect(calc(2, '+', 5)).toBe(7);
    expect(calc(-1, '+', -2)).toBe(-3);
    expect(calc(-2, '+', -1)).toBe(-3);
  });

  it('should handle substraction', () => {
    expect(calc(0, '-', 0)).toBe(0);
    expect(calc(-1, '-', 1)).toBe(-2);
    expect(calc(2, '-', 5)).toBe(-3);
    expect(calc(-1, '-', -2)).toBe(1);
    expect(calc(-2, '-', -1)).toBe(-1);
  });

  it('should handle multiplication', () => {
    expect(calc(0, '*', 0)).toBe(0);
    expect(calc(-1, '*', 1)).toBe(-1);
    expect(calc(2, '*', 5)).toBe(10);
    expect(calc(-1, '*', -2)).toBe(2);
    expect(calc(-2, '*', -1)).toBe(2);
  });

  it('should handle division', () => {
    expect(calc(0, '/', 0)).toBe(NaN);
    expect(calc(12, '/', 0)).toBe(Infinity);
    expect(calc(-1, '/', 1)).toBe(-1);
    expect(calc(2, '/', 5)).toBe(0.4);
    expect(calc(-1, '/', -2)).toBe(0.5);
    expect(calc(-2, '/', -1)).toBe(2);
  });
});
