import square from './../../exercises/basics/2';

describe('square (square.js)', () => {
  it('should return the square of the given number', () => {
    expect(square(0)).toBe(0);
    expect(square(-1)).toBe(1);
    expect(square(2)).toBe(4);
    expect(square(5)).toBe(25);
  });
});
