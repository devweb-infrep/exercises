import isPal from './../../exercises/strings/6';

describe('is palindrom (is-pal.js)', () => {
  it('should return true if palindrom', () => {
    expect(isPal('tot')).toBe(true);
    expect(isPal('kayak')).toBe(true);
  });

  it('should return false if not a palindrom', () => {
    expect(isPal('toto')).toBe(false);
    expect(isPal('nope')).toBe(false);
  });
});
