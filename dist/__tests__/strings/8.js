import anagram from './../../exercises/strings/8';

describe('anagram (anagram.js)', () => {
  test('should return true if passed string is an anagram', () => {
    expect(anagram('hello', 'llohe')).toBeTruthy();
    expect(anagram('Whoa! Hi!', 'Hi! Whoa!')).toBeTruthy();
    expect(anagram('RAIL! SAFETY!', 'fairy tales')).toBeTruthy();
  });

  test('should return false if passed string is not an anagram', () => {
    expect(anagram('One One', 'Two two two')).toBeFalsy();
    expect(anagram('One one', 'One one c')).toBeFalsy();
    expect(
      anagram('A tree, a life, a bench', 'A tree, a fence, a yard')
    ).toBeFalsy();
  });
});
