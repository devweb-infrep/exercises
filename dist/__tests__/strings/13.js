import pyramid from './../../exercises/strings/13';

describe('pyramid (pyramid.js)', () => {
  it('should work', () => {
    expect(pyramid(1)).toBe('#');
    expect(pyramid(2)).toBe(` # \n###`);
    expect(pyramid(3)).toBe(`  #  \n ### \n#####`);
  });
});
