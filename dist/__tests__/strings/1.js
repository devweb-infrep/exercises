import upperFirst from './../../exercises/strings/1';

describe('upperFirst (upper-first.js)', () => {
  it('should uppercase the first letter', () => {
    expect(upperFirst('toto')).toBe('Toto');
    expect(upperFirst('TOTO')).toBe('TOTO');
    expect(upperFirst('toto TOTO')).toBe('Toto TOTO');
  });
});
