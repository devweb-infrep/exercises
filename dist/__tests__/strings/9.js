import longestWord from './../../exercises/strings/9';

describe('longestWord (longest-word.js)', () => {
  test('should return first, longest word in passed string', () => {
    expect(longestWord('Hello there')).toEqual('Hello');
    expect(longestWord('My name is Adam')).toEqual('name');
    expect(longestWord('fun&!! time')).toEqual('time');
  });
});
