import wordsCount from './../../exercises/strings/2';

describe('wordsCount (words-count.js)', () => {
  it('should count the number of words', () => {
    expect(wordsCount('Combien de mots')).toBe(3);
  });

  it('should ignore the punctuation', () => {
    expect(wordsCount('Et la, combien de mots ?')).toBe(5);
  });
});
