import steps from './../../exercises/strings/11';

describe('steps (steps.js)', () => {
  it('should work', () => {
    expect(steps(1)).toBe('#');
    expect(steps(2)).toBe(`#
##`);

    expect(steps(3)).toBe(`#
##
###`);
  });
});
