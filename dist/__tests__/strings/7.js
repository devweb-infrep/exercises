import repeat from './../../exercises/strings/7';

describe('repeat (repeat.js)', () => {
  it('should work', () => {
    expect(repeat('aze', 3)).toBe('azeazeaze');
  });

  it('should handle invalid repeat number', () => {
    expect(repeat('aze', 0)).toBe('');
    expect(repeat('aze', -1)).toBe('');
  });
});
