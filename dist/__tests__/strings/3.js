import vowelCount from './../../exercises/strings/3';

describe('vowelCount (vowel-count.js)', () => {
  it('should return the count of vowels', () => {
    expect(vowelCount('abcde')).toBe(2);
  });

  it('should handle uppercased letters', () => {
    expect(vowelCount('Et la ?')).toBe(2);
  });

  test('should return correct number of vowels in given string', () => {
    expect(vowelCount('aeiou')).toEqual(5);
    expect(vowelCount('Adam')).toEqual(2);
    expect(vowelCount('Hello there!')).toEqual(4);
  });
});
