import invert from './../../exercises/strings/4';

describe('invert (invert.js)', () => {
  it('should invert the given string', () => {
    expect(invert('vercingetorix')).toBe('xirotegnicrev');
  });
});
