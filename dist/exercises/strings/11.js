/**
 * @todo Write a function that accepts a positive number N and returns a step shape
 * with N levels using the '#' character.
 *
 * @param {number} size the number of steps
 *
 * @return {string} the steps
 *
 * @example
 * steps(2) = '# '
 *            '##'
 *
 * steps(3) = '#  '
 *            '## '
 *            '###'
 *
 * steps(4) = '#   '
 *            '##  '
 *            '### '
 *            '####'
 *
 */
export default function(size) {
  // Do not write any code above this line
  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------

  // Write your code here (only between the lines)

  // Assign your result to this variable
  var result;

  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------
  // Do not write any code below this line
  return result;
}
