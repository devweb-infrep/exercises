/**
 * @todo Pig Latin is a children's language that is intended to be confusing when
 * spoken quickly. Your job for this exercise is to create a solution that takes
 * the words given and turns them into pig latin. Please see the following
 * wikipedia page for details regarding the rules of Pig Latin:
 *
 * @see https://fr.m.wikipedia.org/wiki/Pig_latin_(linguistique)
 *
 * @param {array} words the phrase to be pig-latinized
 *
 * @return {string} the pig latin string
 */
export default function(words) {
  // Do not write any code above this line
  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------

  // Write your code here (only between the lines)

  // Assign your result to this variable
  var result;

  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------
  // Do not write any code below this line
  return result;
}
