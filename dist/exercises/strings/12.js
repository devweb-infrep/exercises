/**
 * @todo In cryptography, a Caesar cipher, also known as Caesar's cipher,
 * the shift cipher, Caesar's code or Caesar shift, is one of the simplest and
 * most widely known encryption techniques. It is a type of substitution cipher
 * in which each letter in the plaintext is replaced by a letter some fixed
 * number of positions down the alphabet. For example, with a left shift of 3,
 * D would be replaced by A, E would become B, and so on.
 *
 * The method is named after Julius Caesar, who used it in his private correspondence.
 *
 * @param {string} message the message before encryption
 *
 * @return {string} the encoded message
 */
export default function(message, shift) {
  // Do not write any code above this line
  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------

  // Write your code here (only between the lines)

  // Assign your result to this variable
  var result;

  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------
  // Do not write any code below this line
  return result;
}
