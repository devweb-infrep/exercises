/**
 * @todo  Write a function that accepts a positive number N and returns a pyramid shape with N levels using the # character.
 *
 * Examples:
 * pyramid(1) = '#'
 *
 * pyramid(2) = ' # '
 *              '###'
 *
 * pyramid(3) = '  #  '
 *              ' ### '
 *              '#####'
 *
 * pyramid(4) = '   #   '
 *              '  ###  '
 *              ' ##### '
 *              '#######'
 *
 * @param {number} height the size (height) of the pyramid
 *
 * @return {string} the pyramid of #
 */
export default function(height) {
  // Do not write any code above this line
  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------

  // Write your code here (only between the lines)

  // Assign your result to this variable
  var result;

  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------
  // Do not write any code below this line
  return result;
}
