/**
 * @todo Write a function that, given a N number, returns a NxN spiral matrix
 
 * @param {number} size The size of the matrix 
 *
 * @return {array} the spiral matrix
 * 
 * @example
 * matrix(2) = [[1, 2],
 *              [4, 3]]
 *
 * matrix(3) = [[1, 2, 3],
 *              [8, 9, 4],
 *              [7, 6, 5]]
 *
 * matrix(4) = [[1,   2,  3, 4],
 *              [12, 13, 14, 5],
 *              [11, 16, 15, 6],
 *              [10,  9,  8, 7]]
 */

export default function(size) {
  // Do not write any code above this line
  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------

  // Write your code here (only between the lines)

  // Assign your result to this variable
  var result;

  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------
  // Do not write any code below this line
  return result;
}
