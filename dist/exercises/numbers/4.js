/**
 * @todo Create a function that returns a specific member of the fibonacci sequence:
 * a series of numbers in which each number ( Fibonacci number ) is the sum of the
 * two preceding numbers. The simplest is the series 1, 1, 2, 3, 5, 8, etc.
 *
 * @example
 * fibonacci(4) // returns the 4th member of the series: 3  (1, 1, 2, 3)
 * fibonacci(6) // returns 8
 *
 * @param {number} index the index in the fibonacci suite
 *
 * @return {number} the correct number
 */
export default function(index) {
  // Do not write any code above this line
  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------

  // Write your code here (only between the lines)

  // Assign your result to this variable
  var result;

  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------
  // Do not write any code below this line
  return result;
}
