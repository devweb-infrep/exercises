/**
 * @todo Create a function that determines whether or not a given year is a leap
 * year. Leap years are determined by the following rules:
 *
 * There is a leap year every year whose number is perfectly divisible by four -
 * except for years which are both divisible by 100 and not divisible by 400.
 * The second part of the rule effects century years.
 *
 * For example; the century years 1600 and 2000 are leap years, but the century
 * years 1700, 1800, and 1900 are not.
 *
 * @param {number} year
 *
 * @return {number} Wether the given year is a leap year or not
 */
export default function(year) {
  // Do not write any code above this line
  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------

  // Write your code here (only between the lines)

  // Assign your result to this variable
  var result;

  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------
  // Do not write any code below this line
  return result;
}
