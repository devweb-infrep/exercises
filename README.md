# Javascript Exercices

## Installation

```
# clone the public repo
git clone https://papswell@bitbucket.org/devweb-infrep/exercises.git

# install dependencies
npm install
```

## Doing the exercises

The exercises are at `dist/exercises`.
They are gathered by themes and sorted in each folder by order of difficulty. (The bigger the number, the more difficult)

You have to write your code between the marks. **Do not touch any other code inside the exercises files, otherwise things will break.**

For every exercises, you have to assign your final value to the variable `result`. (If you know what you are doing, you can `return` your own variable if you prefer, just make sure to return your result)

### Use the power of git !

Do not hesitate to create branches when trying to solve the exercises, so you can keep a track of your progression, and makes tries without breaking anything else. You can also publish your branches to ask some help, or to show your solution if anyone asks for it.

### Use the corrector

Anythime your want to test your solution, just do in the terminal :

```
npm start
```

This command will test your code, and show you an error message if something went wrong :** Make sure to read the error carefully !**

It can be a real error if your code is not valid, or a mistake in your logic.  
If the code is invalid, it's easy to fix, Visual Studio will show you the mistake in red.
If the code is valid but you have an error, the message will show you what was the expected result, and what it actually received. Breathe, take a break, then keep trying !

When the exercise is completed, you will see a message in green, and you can move to the next exercise. Congrats :)

#### Note :

Running `npm start` will auto correct the whole suite of exercises. If you want to work on a specific subject, you can filter the exercises like so :

```
# Run only exercises in strings folder
npm start -- strings

# Run only exercise 2 in strings folder
npm start -- strings/2.js
```
