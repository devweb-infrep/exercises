#!/usr/bin/env node
var inquirer = require('inquirer');

const Exercise = require('./../lib/models/exercise');
const Param = require('./../lib/models/param');

const writeExercicse = require('./../lib/add-exercise');

const askWhatToDo = exercise => {
  return inquirer.prompt({
    type: 'input',
    name: 'todo',
    message: 'What needs to be done in this exercise ?',
    validate: answer => !!answer,
  });
};

const askForReturn = exercise => {
  return inquirer.prompt([
    {
      type: 'input',
      name: 'returnDesc',
      message: 'What does the function must return ?',
      validate: answer => !!answer,
    },
    {
      type: 'list',
      choices: ['string', 'number', 'bool', 'array', 'object'],
      name: 'returnType',
      message: 'What is the type of the value returned by the function ?',
      validate: answer => !!answer,
    },
  ]);
};

const askForParam = exercise => {
  return inquirer
    .prompt({
      type: 'confirm',
      name: 'addParam',
      message: 'Add a parameter to the function ?',
    })
    .then(answer => {
      if (!answer.addParam) {
        return Promise.resolve();
      }

      return askCreateParam(exercise);
    });
};

const askCreateParam = exercise => {
  return inquirer
    .prompt([
      {
        type: 'input',
        name: 'name',
        message: 'What is the name of the parameter ?',
        validate: answer => {
          if (!answer) {
            return 'Parameter must have a name.';
          }

          if (exercise.hasParam(answer)) {
            return `This exercise already has a parameter named : ${chalk.bold(
              answer
            )}.`;
          }

          return true;
        },
      },
      {
        type: 'list',
        choices: ['string', 'number', 'bool', 'array', 'object'],
        name: 'type',
        message: 'What is the type of the parameter ?',
        validate: answer => !!answer,
      },
      {
        type: 'input',
        name: 'desc',
        message: 'Describe this parameter ?',
      },
    ])
    .then(answers => {
      const p = new Param(answers.name, answers.type, answers.desc);
      exercise.addParam(p);

      return askForParam(exercise);
    });
};

module.exports = name => {
  let exercise = new Exercise(name);

  return askWhatToDo(exercise)
    .then(answers => {
      exercise.toDo(answers.todo);
      return askForReturn();
    })
    .then(answers => {
      exercise.addReturn(answers.returnType, answers.returnDesc);
      return askForParam(exercise);
    })
    .then(() => {
      writeExercicse(exercise);
      console.log('Exercise created. Please implement the tests now. ');
    });
};
