#!/usr/bin/env node
var program = require('commander');

program
  .command('create <exercise>')
  .description('Create a new exercise')
  .action(function(ex) {
    require('./create-exercise')(ex);
  });

program.parse(process.argv);
