const chalk = require('chalk');

const expectedRegex = new RegExp(/Expected:\s(.*)\n/);
const receivedRegex = new RegExp(/Received:\s(.*)\n/);
const messageRegex = new RegExp(/Difference:\s(.*)\n/);
const paramsRegex = new RegExp(/[\w]+\((.*)\)/);
const callRegex = new RegExp(/>\s*[0-9]+\s\|\s*expect\((.*)\)\./);

const getFileName = string =>
  string
    .replace(/\r?\n|\r/g, '')
    .replace(/\s*src\/([\w-]+\/)__tests__\/\s*/, (matchMedia, p1) => {
      return p1;
    })
    .replace(/\s*dist\/__tests__\/([\w-]+\/)\s*/, (matchMedia, p1) => {
      return p1;
    });

const trimJestReportEnd = string => string.replace(/Test Suites:.*/s, '');

const handleData = (message, line) => {
  let expected = expectedRegex.exec(line);
  let received = receivedRegex.exec(line);
  if (!expected || !received) {
    message += `
${chalk.red('● An error has occured while executing the function')}

    ${line
      .split('\n')
      .slice(2)
      .join('\n')}`;
  } else {
    expected = expected[1];
    received = received[1];

    const _call = callRegex.exec(line);
    if (_call) {
      const call = _call[1];
      const params = paramsRegex.exec(call)[1];
      message += `
● Calling function with parameters : ${chalk.bold(params)}
  `;
    } else {
      console.log('LINE', line);
    }

    if (received === 'undefined') {
      message += chalk.yellow(
        `WARNING:    
      The function call returned "undefined".
      Make sure that you didn\'t forget to use "return" at the end of your function.
  `
      );
    }

    message += chalk.visible(`Error while running test :
    ${chalk.visible(`Expected result: ${chalk.green(expected)}`)} 
    ${chalk.visible(`Received result: ${chalk.red(received)}`)}
`);
  }

  return message;
};

const handleError = error => {
  let successes = [];
  const errorMessage = error.message
    .replace(/Command failed.*/, '')
    .split('FAIL')
    .slice(1)
    .map(testFile => {
      let parts = testFile.split('●');

      parts = parts.map((part, i, array) => {
        const split = part.split('PASS');
        const success = split.slice(1);

        successes = successes.concat(success);
        return split[0];
      });

      parts[parts.length - 1] = trimJestReportEnd(parts[parts.length - 1]);
      return {
        file: parts[0],
        content: parts.slice(1),
      };
    })
    .map(data => {
      const filename = getFileName(data.file);

      const message = `
-------------------------------------------------------------------------
${filename}
-------------------------------------------------------------------------`;

      return data.content.reduce(handleData, message);
    })
    .join('');

  console.log(errorMessage);

  const successMessage = successes
    .map(data => {
      const filename = getFileName(data);

      return `
-------------------------------------------------------------------------
${trimJestReportEnd(filename)}
-------------------------------------------------------------------------
${chalk.green('Correct !')}
`;
    })
    .join('');
  console.log(successMessage);
};

module.exports = handleError;
