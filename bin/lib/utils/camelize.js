const camelize = str =>
  str.replace(/(\-+[a-z])/g, s => s.replace(/-+/g, '').toUpperCase());

module.exports = camelize;
