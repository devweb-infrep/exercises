const Exercise = {
  name: '',
  params: [],
  todo: '',
  return: '',
  toDo: function(consign) {
    this.todo = consign;
    return this;
  },
  hasParam: function(paramName) {
    return this.params.some(p => p.name === paramName);
  },
  addParam: function(param) {
    if (this.hasParam(param.name)) {
      throw new TypeError('PARAM_ALREADY_EXISTS');
    }

    this.params.push(param);
    return this;
  },
  addReturn: function(type, desc) {
    this.return = type ? `{${type}} ${desc}` : desc;
    return this;
  },
};

module.exports = function(name) {
  const e = Object.create(Exercise);
  e.name = name;
  return e;
};
