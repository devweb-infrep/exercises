const Param = {
  name: '',
  type: '',
  description: '',
  toString: function() {
    return `{${this.type}} ${this.name} ${this.description}`;
  },
  setName: function(name) {
    this.name = name;
    return this;
  },
  setDescription: function(description) {
    this.description = description;
    return this;
  },
  setType: function(type) {
    this.type = type;
    return this;
  },
};

module.exports = function(name, type, description) {
  const p = Object.create(Param);
  p.setName(name)
    .setDescription(description)
    .setType(type);

  return p;
};
