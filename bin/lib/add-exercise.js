const fs = require('fs');
const path = require('path');
const camelize = require('./utils/camelize');

const addExercise = (exercise, _path) => {
  let content = fs
    .readFileSync(path.resolve(__dirname, './../_templates/exercise'))
    .toString();

  let testContent = fs
    .readFileSync(path.resolve(__dirname, './../_templates/test'))
    .toString();

  if (exercise.todo) {
    content = content.replace('%todo%', exercise.todo);
  }

  if (!exercise.params || !exercise.params.length) {
    content = content.replace('%params%', '');
  } else {
    let paramsString = '';
    exercise.params.forEach(p => {
      paramsString = paramsString.concat(` * @param ${p} \n`);
    });

    paramString = paramsString.concat(' *\n * @return');

    content = content
      .replace(' * @return', paramString)
      .replace('%params%', exercise.params.map(p => p.name).join(', '));
  }

  if (exercise.return) {
    content = content.replace('%return%', exercise.return);
  }

  fs.writeFileSync(
    path.resolve(__dirname, `./../../src/${exercise.name}.js`),
    content
  );

  const parts = exercise.name.split('/');
  const file = parts[parts.length - 1];
  const folder = parts.slice(0, parts.length - 1).join('/');

  testContent = testContent
    .replace(/%function%/g, camelize(file))
    .replace(/%file%/g, file)
    .replace('%it%', 'should work');

  fs.writeFileSync(
    path.resolve(__dirname, `./../../src/${folder}/__tests__/${file}.js`),
    testContent
  );
};

module.exports = addExercise;
