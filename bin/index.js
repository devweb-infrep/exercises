#!/usr/bin/env node

const exec = require('child_process').exec;
const chalk = require('chalk');
const handleError = require('./lib/parser/jest-report-parser');

const folder = process.argv.slice(2)[0];
const command = `jest dist/__tests__${folder ? `/${folder}` : ''}`;

exec(command, function(err, stdo, stde) {
  if (stdo && stdo.includes('No tests found')) {
    console.error(
      chalk.red('No exercices found at the given path : ' + folder)
    );
  } else {
    if (err) {
      handleError(err);
    } else {
      console.log(chalk.green('All exercices are correct. Well done :)'));
    }
  }
  process.exit(0);
});
