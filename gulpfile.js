var path = require('path');
var chalk = require('chalk');
var gulp = require('gulp');
var rename = require('gulp-rename');
var replace = require('gulp-replace');

const regex = new RegExp(`\\${path.sep}__tests__`, 'g');

const exercises = require('./exercises.json');

const IMPLEMENTATION_DELIMITER = `// ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------`;

const IMPLEMENTATION_REGEX = /\/\/\s*[-]*\s*\/\/\s*[-]*\s*(.*)\/\/\s*[-]*\s*\/\/\s*[-]+/s;
const IMPORT_REGEX = /import\s*([\w_]*)\s*from\s*'((\.?\.\/)*)([\w-_]+)'/;

gulp.task('build', function() {
  return gulp
    .src('./src/**/*.js')
    .pipe(
      rename((path, args) => {
        const dirname = path.dirname.replace(regex, '');
        const scopedExercises = exercises[dirname];

        if (!scopedExercises) return;

        if (path.dirname.includes('__tests__')) {
          path.dirname = '__tests__/' + dirname;
        } else {
          path.dirname = 'exercises/' + dirname;
        }

        const index = scopedExercises.indexOf(path.basename);

        if (index < 0) {
          throw new TypeError(
            `EXERCISE NOT REGISTERED (exercices.${dirname}.${path.basename})`
          );
        }

        path.basename = index + 1;
      })
    )
    .pipe(
      // See https://github.com/gulpjs/vinyl#instance-properties for details on available properties
      replace(
        IMPLEMENTATION_REGEX,
        `// ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------

  // Write your code here (only between the lines)

  // Assign your result to this variable
  var result;

  // ---------------------------------------------------------------------------
  // ---------------------------------------------------------------------------`
      )
    )
    .pipe(
      replace(IMPORT_REGEX, function(match, p1, p2) {
        const parts = this.file.path.split(path.sep).slice(-2);

        const folder = parts[0];
        const index = parts[1].split('.')[0];

        return `import ${p1} from '${p2}../exercises/${folder}/${index}'`;
      })
    )
    .pipe(gulp.dest('./dist'));
});

gulp.task('default', function() {
  // return gulp.watch('./src/**/*.js', function(obj) {
  // });
});
